# Deployment of your own development environment

* Create a project/namespace to work in.
  In OpenShift: `oc new-project my-project`
  In Kubernetes: `kubectl create namespace my-project ; kubectl config set-context --current --namespace=my-namespace`
* Deploy an emply Elasticsearch: `kubectl apply -f elastic-deployment.yaml`
* Populate Elasticsearch with necessary indexes(to be automated):
```shell
 kubectl exec =$(kubectl get pods -l app=elastic -o=jsonpath='{.items[0].metadata.name}') -- curl -X PUT localhost:9200/platsannons-read
  kubectl exec =$(kubectl get pods -l app=elastic -o=jsonpath='{.items[0].metadata.name}') -- curl -X PUT localhost:9200/jae-synonym-dictionary
```
* Deploy castaway appen: `kubectl apply -f castaway-deployment.yaml`
* If running OpenShift deploy a route to the service. `kubectl apply -f route.yaml`
